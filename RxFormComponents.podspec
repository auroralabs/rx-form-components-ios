#
# Be sure to run `pod lib lint RxFormComponents.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'RxFormComponents'
    s.version          = '0.3.2'
    s.summary          = 'Librería de componentes para formularios'

    s.description      = <<-DESC
    Librería de componentes para formularios
    DESC

    s.homepage         = 'https://bitbucket.org/auroralabs/rx-form-components-ios'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'Álvaro Murillo' => 'alvaro.murillop@auroralabs.es' }
    s.source           = { :git => 'https://bitbucket.org/auroralabs/rx-form-components-ios', :tag => s.version.to_s }
    s.swift_version = '5.1'
    s.platform = :ios, '11.0'

    s.source_files = 'Sources/**/*'
    s.resource_bundles = {
        'RxFormComponents' => ['Resources/**/*']
    }

    s.frameworks = 'UIKit'
    s.dependency 'RxSwift'
    s.dependency 'RxCocoa'
end
