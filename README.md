# RxFormComponents

[![CI Status](https://img.shields.io/travis/Álvaro Murillo/RxFormComponents.svg?style=flat)](https://travis-ci.org/Álvaro Murillo/RxFormComponents)
[![Version](https://img.shields.io/cocoapods/v/RxFormComponents.svg?style=flat)](https://cocoapods.org/pods/RxFormComponents)
[![License](https://img.shields.io/cocoapods/l/RxFormComponents.svg?style=flat)](https://cocoapods.org/pods/RxFormComponents)
[![Platform](https://img.shields.io/cocoapods/p/RxFormComponents.svg?style=flat)](https://cocoapods.org/pods/RxFormComponents)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RxFormComponents is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RxFormComponents', :git => 'https://bitbucket.org/auroralabs/rx-form-components-ios.git', :tag => '0.3.0'

# Add this to render @IBDesignable in your storyboards
post_install do |installer|
    installer.pods_project.build_configurations.each do |config|
        next unless config.name == 'Debug'
        config.build_settings['LD_RUNPATH_SEARCH_PATHS'] = ['$(FRAMEWORK_SEARCH_PATHS)']
    end
end
```

## Author

Álvaro Murillo, alvaromurillop@gmail.com

## License

RxFormComponents is available under the MIT license. See the LICENSE file for more info.
