import Foundation
import UIKit

public enum InputType {
    case textField(TextFieldConfig)
    case datePicker(DatePickerConfig)
}

public extension InputType {
    var inputView: UIView {
        switch self {
        case let .textField(config):
            let textField = ValidatableTextField()
            textField.configure(config)
            return textField
        case let .datePicker(config):
            let datePicker = ValidatableDatePicker()
            datePicker.configure(config: config)
            return datePicker
        }
    }
}

