import UIKit

public struct KeyboardConfig {
    public let autocapitalizationType: UITextAutocapitalizationType?
    public let isSecureTextEntry: Bool?
    public let keyboardType: UIKeyboardType?
    public let spellCheckingType: UITextSpellCheckingType?
    public let autocorrectionType: UITextAutocorrectionType?
    public let contentType: UITextContentType?
}

public extension KeyboardConfig {

    private static func build(
        autocapitalizationType: UITextAutocapitalizationType? = nil,
        isSecureTextEntry: Bool? = nil,
        keyboardType: UIKeyboardType? = nil,
        spellCheckingType: UITextSpellCheckingType? = nil,
        autocorrectionType: UITextAutocorrectionType? = nil,
        contentType: UITextContentType? = nil
    ) -> KeyboardConfig {
        .init(
            autocapitalizationType: autocapitalizationType,
            isSecureTextEntry: isSecureTextEntry,
            keyboardType: keyboardType,
            spellCheckingType: spellCheckingType,
            autocorrectionType: autocorrectionType,
            contentType: contentType
        )
    }

    static var name: KeyboardConfig {
        .build(autocapitalizationType: .words)
    }

    static var sentence: KeyboardConfig {
        .build(autocapitalizationType: .sentences)
    }

    static var password: KeyboardConfig {
        .build(
            isSecureTextEntry: true,
            contentType: .password
        )
    }

    static var email: KeyboardConfig {
        .build(
            autocapitalizationType: UITextAutocapitalizationType.none,
            keyboardType: .emailAddress,
            spellCheckingType: .no,
            autocorrectionType: .no,
            contentType: .emailAddress
        )
    }

    static var number: KeyboardConfig {
        .build(keyboardType: .numberPad)
    }

    static var code: KeyboardConfig {
        .build(
            autocapitalizationType: UITextAutocapitalizationType.none,
            spellCheckingType: .no,
            autocorrectionType: .no
        )
    }
    
    static var oneTimeCode: KeyboardConfig {
        if #available(iOS 12.0, *) {
            return .build(
                autocapitalizationType: UITextAutocapitalizationType.none,
                spellCheckingType: .no,
                autocorrectionType: .no,
                contentType: .oneTimeCode
            )
        } else {
            return .build(
                autocapitalizationType: UITextAutocapitalizationType.none,
                spellCheckingType: .no,
                autocorrectionType: .no
            )
        }
    }

    static var numbersAndPunctuation: KeyboardConfig {
        .build(
            keyboardType: .numbersAndPunctuation,
            spellCheckingType: .no,
            autocorrectionType: .no
        )
    }

    static var url: KeyboardConfig {
        .build(
            autocapitalizationType: UITextAutocapitalizationType.none,
            keyboardType: .URL,
            spellCheckingType: .no,
            autocorrectionType: .no,
            contentType: .URL
        )
    }

    static var phone: KeyboardConfig {
        .build(
            autocapitalizationType: UITextAutocapitalizationType.none,
            keyboardType: .phonePad,
            spellCheckingType: .no,
            autocorrectionType: .no,
            contentType: .telephoneNumber
        )
    }

    static func other(
        autocapitalizationType: UITextAutocapitalizationType? = nil,
        isSecureTextEntry: Bool? = nil,
        keyboardType: UIKeyboardType? = nil,
        spellCheckingType: UITextSpellCheckingType? = nil,
        autocorrectionType: UITextAutocorrectionType? = nil
    ) -> KeyboardConfig {
        .build(
            autocapitalizationType: autocapitalizationType,
            isSecureTextEntry: isSecureTextEntry,
            keyboardType: keyboardType,
            spellCheckingType: spellCheckingType,
            autocorrectionType: autocorrectionType
        )
    }
}
