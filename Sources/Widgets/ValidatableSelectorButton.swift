import UIKit
import RxCocoa
import RxSwift

@IBDesignable public class ValidatableSelectorButton: UIView {
    @objc dynamic public var style: FormStyle {
        get { currentStyle }
        set { currentStyle = newValue }
    }

    private var currentStyle = FormStyle() {
        didSet { applyStyle() }
    }
    
    public var label: String? { didSet { topLabel.text = label; invalidateIntrinsicContentSize() } }

    public var error: String? {
        didSet {
            errorLabel.text = error
            button.isError = !(error?.isEmpty ?? true)
            invalidateIntrinsicContentSize()
        }
    }

    public let button: SelectorButton = {
        let button = SelectorButton(type: .custom)
        return button
    }()
    public var placeholder: String? {
        set { button.placeholder = newValue }
        get { button.placeholder }
    }
    public func set(value: String?) {
        button.value = value
    }

    private lazy var topLabel: UILabel = .top(style: currentStyle)
    private lazy var errorLabel: UILabel = .error(style: currentStyle)

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }

    private func setUp() {
        addSubview(topLabel)
        addSubview(button)
        addSubview(errorLabel)

        #if TARGET_INTERFACE_BUILDER
        label = "Field"
        placeholder = "Placeholder"
        #endif
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        topLabel.frame = .init(origin: .zero, size: .init(width: bounds.width, height: topLabelHeight))
        button.frame = .init(
            origin: topLabel.frame.bottomLeft + CGPoint(y: currentStyle.labelMargin),
            size: .init(width: bounds.width, height: currentStyle.inputHeight)
        )
        errorLabel.frame = .init(
            origin: button.frame.bottomLeft + CGPoint(y: currentStyle.errorMargin),
            size: .init(width: bounds.width, height: errorHeight)
        )
    }

    private var topLabelHeight: CGFloat {
        topLabel.sizeThatFits(.init(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)).height
    }

    private var errorHeight: CGFloat {
        errorLabel.sizeThatFits(.init(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)).height
    }

    public override var intrinsicContentSize: CGSize {
        if error != nil {
            return .init(
                width: UIView.noIntrinsicMetric,
                height: topLabelHeight +
                    currentStyle.labelMargin +
                    currentStyle.inputHeight +
                    currentStyle.errorMargin +
                    errorHeight
            )
        } else {
            return .init(
                width: UIView.noIntrinsicMetric,
                height: topLabelHeight + currentStyle.labelMargin + currentStyle.inputHeight
            )
        }
    }
    
    private func applyStyle() {
        topLabel.textColor = currentStyle.topLabelColor
        topLabel.font = currentStyle.topLabelFont
        errorLabel.textColor = currentStyle.errorColor
        errorLabel.font = currentStyle.errorLabelFont
        
        button.currentStyle = currentStyle
        
        setNeedsLayout()
        invalidateIntrinsicContentSize()
    }
}

public class SelectorButton: UIButton {
    var currentStyle = FormStyle() {
        didSet { applyStyle() }
    }
    
    var isError: Bool = false {
        didSet { layer.borderColor = isError ? currentStyle.errorColor.cgColor : currentStyle.borderColor.cgColor }
    }

    var value: String? { didSet { updateTitle() } }

    var placeholder: String? { didSet { updateTitle() } }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }

    private func setUp() {
        titleLabel?.lineBreakMode = .byTruncatingTail
        layer.borderWidth = 1.0
        contentHorizontalAlignment = .left
        setImage(UIImage.disclosureIcn, for: .normal)
        applyStyle()
    }

    private func updateFontColor() {
        setTitleColor(value != nil ? .black : currentStyle.placeholderColor, for: .normal)
    }

    private func updateTitle() {
        title = value ?? placeholder
        updateFontColor()
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

        let imageWidth = imageView?.image?.size.width ?? 0
        let leftImageInset: CGFloat = (bounds.width - currentStyle.inputInsets.right - currentStyle.inputInsets.left - imageWidth)
        let leftTitleInset: CGFloat = -imageWidth
        contentEdgeInsets = currentStyle.inputInsets
        imageEdgeInsets = .init(top: 0, left: leftImageInset, bottom: 0, right: -leftImageInset)
        titleEdgeInsets = .init(top: 0, left: leftTitleInset, bottom: 0, right: -leftTitleInset)
    }
    
    private func applyStyle() {
        titleLabel?.font = currentStyle.inputFont
        layer.cornerRadius = currentStyle.cornerRadius
        layer.borderColor = currentStyle.borderColor.cgColor
        updateTitle()
    }
}


extension Reactive where Base: ValidatableSelectorButton {
    public var error: Binder<String?> {
        Binder(self.base) { $0.error = $1 }
    }

    public var value: Binder<String?> {
        Binder(self.base) { $0.set(value: $1) }
    }
}

extension FormInput where T: InputValueRepresentable {
    public func drive(_ button: ValidatableSelectorButton) -> Disposable {
        CompositeDisposable(
            error.drive(button.rx.error),
            value.map { $0?.valueRepresentation }.drive(button.rx.value)
        )
    }
}
