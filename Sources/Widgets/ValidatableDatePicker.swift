import UIKit
import RxCocoa
import RxSwift

public class ValidatableDatePicker: BaseTextFieldContainer {

    private let disposeBag = DisposeBag()
    public let datePicker = UIDatePicker()
    fileprivate let outputSelectedDateRelay: BehaviorRelay<Date?> = .init(value: nil)
    fileprivate let inputSelectedDateRelay: BehaviorRelay<Date?> = .init(value: nil)

    public var date: Date? {
        didSet { if let date = date, datePicker.date != date { datePicker.date = date }; updateTextField() }
    }

    public var isEnabled: Bool {
        set { textField.isEnabled = newValue }
        get { textField.isEnabled }
    }

    public lazy var formatter: DateFormatter = datePicker.datePickerMode.defaultFormatter {
        didSet { updateTextField() }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }

    private func setUp() {
        textField.inputView = datePicker
        textField.clearButtonMode = .always
        addRightButton(type: .dropDown, target: self, action: #selector(rightButtonTapped))
        if #available(iOS 13.4, *) { datePicker.preferredDatePickerStyle = .wheels }
        updateTextField()

        inputSelectedDateRelay.distinctUntilChanged().bind { [weak self] date in
            guard let self = self else { return }
            self.date = date
        }.disposed(by: disposeBag)

        datePicker.rx.date.skip(1).bind { [weak self] date in
            guard let self = self else { return }
            self.date = date
            self.outputSelectedDateRelay.accept(date)
        }.disposed(by: disposeBag)

        textField.rx.controlEvent(.editingDidBegin).bind { [weak self] _ in
            self?.date = self?.datePicker.date
            self?.outputSelectedDateRelay.accept(self?.date)
        }.disposed(by: disposeBag)
    }
    
    @objc private func rightButtonTapped() {
        textField.becomeFirstResponder()
    }

    private func updateTextField() {
        guard let date = date else { textField.text = nil; return }
        textField.text = formatter.string(from: date)
    }

    public func configure(config: DatePickerConfig) {
        label = config.label
        placeholder = config.placeholder
        datePicker.datePickerMode = config.mode
        formatter = config.formatter ?? config.mode.defaultFormatter
        datePicker.minimumDate = config.minimunDate
        datePicker.maximumDate = config.maximumDate
    }
}

extension UIDatePicker.Mode {
    var defaultFormatter: DateFormatter {
        switch self {
        case .date: return DateFormatter(dateStyle: .short, timeStyle: .none)
        case .dateAndTime: return DateFormatter(dateStyle: .short, timeStyle: .short)
        case .time: return DateFormatter(dateStyle: .none, timeStyle: .short)
        case .countDownTimer: return DateFormatter(dateStyle: .none, timeStyle: .short)
        @unknown default: fatalError()
        }
    }
}

extension Reactive where Base: ValidatableDatePicker {
    public var date: Binder<Date?> {
        Binder(self.base) { $0.date = $1 }
    }
}

extension FormInput where T == Date {
    public func drive(_ picker: ValidatableDatePicker) -> Disposable {
        CompositeDisposable(
            error.drive(picker.rx.error),
            value.drive(picker.inputSelectedDateRelay),
            picker.outputSelectedDateRelay.skip(1).bind(onNext: { [weak self] value in self?.didChange(value: value) })
        )
    }
}
