import UIKit
import RxCocoa
import RxSwift

open class BaseTextFieldContainer: UIView {
    @objc dynamic public var style: FormStyle {
        get { currentStyle }
        set { currentStyle = newValue }
    }

    private var currentStyle = FormStyle() {
        didSet { applyStyle() }
    }
    
    public var label: String? { didSet { topLabel.text = label; invalidateIntrinsicContentSize() } }

    public var error: String? {
        didSet {
            errorLabel.text = error
            textField.isError = !(error?.isEmpty ?? true)
            invalidateIntrinsicContentSize()
        }
    }

    public lazy var textField: TextField = TextField()
    public var placeholder: String? {
        set { textField.placeholder = newValue }
        get { textField.placeholder }
    }
    public var text: String? {
        set { textField.text = newValue }
        get { textField.text }
    }

    private lazy var topLabel: UILabel = .top(style: currentStyle)
    private lazy var errorLabel: UILabel = .error(style: currentStyle)

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }

    private func setUp() {
        addSubview(topLabel)
        addSubview(textField)
        addSubview(errorLabel)

        #if TARGET_INTERFACE_BUILDER
        label = "Label"
        placeholder = "Placeholder"
        #endif
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        topLabel.frame = .init(origin: .zero, size: .init(width: bounds.width, height: topLabelHeight))
        textField.frame = .init(
            origin: topLabel.frame.bottomLeft + CGPoint(y: currentStyle.labelMargin),
            size: .init(width: bounds.width, height: currentStyle.inputHeight)
        )
        errorLabel.frame = .init(
            origin: textField.frame.bottomLeft + CGPoint(y: currentStyle.errorMargin),
            size: .init(width: bounds.width, height: errorHeight)
        )
    }

    private var topLabelHeight: CGFloat {
        topLabel.sizeThatFits(.init(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)).height
    }

    private var errorHeight: CGFloat {
        errorLabel.sizeThatFits(.init(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)).height
    }

    public override var intrinsicContentSize: CGSize {
        if error != nil {
            return .init(
                width: UIView.noIntrinsicMetric,
                height: topLabelHeight + currentStyle.labelMargin + currentStyle.inputHeight + currentStyle.errorMargin + errorHeight
            )
        } else {
            return .init(
                width: UIView.noIntrinsicMetric,
                height: topLabelHeight + currentStyle.labelMargin + currentStyle.inputHeight
            )
        }
    }
    
    private func applyStyle() {
        topLabel.textColor = currentStyle.topLabelColor
        topLabel.font = currentStyle.topLabelFont
        errorLabel.textColor = currentStyle.errorColor
        errorLabel.font = currentStyle.errorLabelFont
        
        textField.currentStyle = currentStyle
        
        setNeedsLayout()
        invalidateIntrinsicContentSize()
    }
}


public class TextField: UITextField {

    var currentStyle = FormStyle() {
        didSet { applyStyle() }
    }
    
    private lazy var placeholderAttributes: [NSAttributedString.Key : Any] = [
        NSAttributedString.Key.foregroundColor: currentStyle.placeholderColor,
        NSAttributedString.Key.font: currentStyle.inputFont
    ]

    var isError: Bool = false {
        didSet { layer.borderColor = isError ? currentStyle.errorColor.cgColor : currentStyle.borderColor.cgColor }
    }

    public override var isEnabled: Bool { didSet { rightView?.isHidden = !isEnabled; updateTextColor() } }

    public weak var nextTextField: UITextField?

    public override var placeholder: String? {
        didSet {
            guard let placeholder = placeholder else { super.attributedPlaceholder = nil; return }
            super.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: placeholderAttributes)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }

    private func setUp() {
        layer.borderWidth = 1.0
        applyStyle()
    }

    private func updateTextColor() {
        textColor = isEnabled ? .black : currentStyle.disabledLabelColor
    }

    public override func textRect(forBounds bounds: CGRect) -> CGRect {
        var rect = bounds.inset(by: currentStyle.inputInsets)
        if let rightView = rightView {
            rect = rect.inset(by: .init(
                                top: 0,
                                left: 0,
                                bottom: 0,
                                right: rightView.bounds.width - currentStyle.inputInsets.right
            ))
        }
        if let leftView = leftView {
            rect = rect.inset(by: .init(
                                top: 0,
                                left: leftView.bounds.width - currentStyle.inputInsets.left,
                                bottom: 0,
                                right: 0
            ))
        }
        return rect
    }

    public override func placeholderRect(forBounds bounds: CGRect) -> CGRect { textRect(forBounds: bounds) }

    public override func editingRect(forBounds bounds: CGRect) -> CGRect { textRect(forBounds: bounds) }
    
    private func applyStyle() {
        font = currentStyle.inputFont
        layer.cornerRadius = currentStyle.cornerRadius
        layer.borderColor = currentStyle.borderColor.cgColor
        updateTextColor()
    }
}


extension Reactive where Base: BaseTextFieldContainer {
    public var error: Binder<String?> {
        Binder(self.base) { $0.error = $1 }
    }
}

public extension BaseTextFieldContainer {

    enum RightButtonType { case info, dropDown }

    func addRightButton(type: RightButtonType, target: Any? = nil, action: Selector? = nil) {
        addRightButton(
            image: type.image,
            insets: .init(top: 8, left: 8, bottom: 8, right: 8),
            target: target,
            action: action
        )
    }

    func addRightButton(image: UIImage?, insets: UIEdgeInsets, target: Any? = nil, action: Selector? = nil) {
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.contentEdgeInsets = insets
        if let target = target, let selector = action {
            button.addTarget(target, action: selector, for: .touchUpInside)
        }
        addRightButton(button: button)
    }

    func addRightButton(button: UIButton) {
        textField.rightView = button
        textField.rightViewMode = .always
    }
}

extension BaseTextFieldContainer.RightButtonType {
    var image: UIImage? {
        switch self {
        case .info: return UIImage.infoIcn
        case .dropDown: return UIImage.dropdownIcn
        }
    }
}
