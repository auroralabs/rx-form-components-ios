import UIKit

public struct DatePickerConfig {
    public let label: String?
    public let placeholder: String?
    public let mode: UIDatePicker.Mode
    public let formatter: DateFormatter?
    public let minimunDate: Date?
    public let maximumDate: Date?

    public init(
        label: String? = nil,
        placeholder: String? = nil,
        mode: UIDatePicker.Mode,
        formatter: DateFormatter? = nil,
        minimunDate: Date? = nil,
        maximumDate: Date? = nil
    ) {
        self.label = label
        self.placeholder = placeholder
        self.mode = mode
        self.formatter = formatter
        self.minimunDate = minimunDate
        self.maximumDate = maximumDate
    }
}
