import UIKit
import RxCocoa
import RxSwift

@IBDesignable open class ValidatableTextView: UIView {
    @objc dynamic public var style: FormStyle {
        get { currentStyle }
        set { currentStyle = newValue }
    }

    private var currentStyle = FormStyle() {
        didSet { applyStyle() }
    }
    
    public var label: String? { didSet { topLabel.text = label; invalidateIntrinsicContentSize() } }

    public var error: String? {
        didSet {
            errorLabel.text = error
            textView.isError = !(error?.isEmpty ?? true)
            invalidateIntrinsicContentSize()
        }
    }

    public let textView: TextView = TextView()
    public var placeholder: String? {
        set { textView.placeholder = newValue }
        get { textView.placeholder }
    }
    public var text: String? {
        set { textView.text = newValue }
        get { textView.text }
    }

    private lazy var topLabel: UILabel = .top(style: currentStyle)
    private lazy var errorLabel: UILabel = .error(style: currentStyle)

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }

    private func setUp() {
        topLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(topLabel)
        textView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(textView)
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(errorLabel)

        #if TARGET_INTERFACE_BUILDER
        label = "Label"
        placeholder = "Placeholder"
        #endif
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        topLabel.frame = .init(origin: .zero, size: .init(width: bounds.width, height: topLabelHeight))
        textView.frame = .init(
            origin: topLabel.frame.bottomLeft + CGPoint(y: currentStyle.labelMargin),
            size: .init(width: bounds.width, height: textHeight)
        )
        errorLabel.frame = .init(
            origin: textView.frame.bottomLeft + CGPoint(y: currentStyle.errorMargin),
            size: .init(width: bounds.width, height: errorHeight)
        )
    }

    private var topLabelHeight: CGFloat {
        topLabel.sizeThatFits(.init(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)).height
    }

    private var errorHeight: CGFloat {
        errorLabel.sizeThatFits(.init(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)).height
    }

    private var textHeight: CGFloat {
        max(currentStyle.textViewMinHeight, textView.contentSize.height)
    }

    public override var intrinsicContentSize: CGSize {
        if error != nil {
            return .init(
                width: UIView.noIntrinsicMetric,
                height: topLabelHeight + currentStyle.labelMargin + textHeight + currentStyle.errorMargin + errorHeight
            )
        } else {
            return .init(
                width: UIView.noIntrinsicMetric,
                height: topLabelHeight + currentStyle.labelMargin + textHeight
            )
        }
    }
    
    private func applyStyle() {
        topLabel.textColor = currentStyle.topLabelColor
        topLabel.font = currentStyle.topLabelFont
        errorLabel.textColor = currentStyle.errorColor
        errorLabel.font = currentStyle.errorLabelFont
        textView.currentStyle = currentStyle
        
        setNeedsLayout()
        invalidateIntrinsicContentSize()
    }
}


public class TextView: UITextView {
    
    var currentStyle = FormStyle() {
        didSet { applyStyle() }
    }
    
    private let disposeBag = DisposeBag()
    private lazy var placeholderLabel: UILabel = .placeholder(style: currentStyle)

    var isError: Bool = false {
        didSet { layer.borderColor = isError ? currentStyle.errorColor.cgColor : currentStyle.borderColor.cgColor }
    }

    public var placeholder: String? { didSet { placeholderLabel.text = placeholder } }

    public override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setUp()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }

    private func setUp() {
        layer.borderWidth = 1.0
        isScrollEnabled = false
        addSubview(placeholderLabel)

        rx.text.bind(
            onNext: { [weak self] _ in
                self?.placeholderLabel.isHidden = !(self?.text.isEmpty ?? true)
            }
        ).disposed(by: disposeBag)

        applyStyle()
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        placeholderLabel.frame = .init(
            origin: CGPoint(x: currentStyle.inputInsets.left, y: currentStyle.inputInsets.top),
            size: placeholderLabel.sizeThatFits(.init(width: bounds.width - currentStyle.inputInsets.left - currentStyle.inputInsets.right, height: CGFloat.greatestFiniteMagnitude))
        )
    }
    
    private func applyStyle() {
        font = currentStyle.inputFont
        layer.cornerRadius = currentStyle.cornerRadius
        layer.borderColor = currentStyle.borderColor.cgColor
        textContainerInset = .init(
            top: currentStyle.inputInsets.top,
            left: currentStyle.inputInsets.left - 4,
            bottom: currentStyle.inputInsets.bottom,
            right: currentStyle.inputInsets.right - 4
        )
    }
}


extension Reactive where Base: ValidatableTextView {
    public var error: Binder<String?> {
        Binder(self.base) { $0.error = $1 }
    }
}

public extension ValidatableTextView {
    func configure(
        label: String? = nil,
        placeholder: String? = nil,
        keyboardConfig: KeyboardConfig,
        delegate: UITextViewDelegate? = nil
    ) {
        textView.configure(keyboard: keyboardConfig)
        textView.delegate = delegate
        self.placeholder = placeholder
        self.label = label
    }

    func configure(_ config: TextFieldConfig) {
        textView.configure(keyboard: config.keyboardConfig)
        self.label = config.label
        self.placeholder = config.placeholder
        textView.returnKeyType = config.returnKey
    }
}


extension FormInput where T == String {
    public func drive(_ textView: ValidatableTextView) -> Disposable {
        CompositeDisposable(
            error.drive(textView.rx.error),
            value.drive(textView.textView.rx.text),
            textView.textView.rx.text.bind(onNext: { [weak self] value in self?.didChange(value: value) }),
            textView.textView.rx.didEndEditing.withLatestFrom(textView.textView.rx.text)
                .bind(onNext: { [weak self] value in self?.didEndEditing(value: value) })
        )
    }
}

extension UITextView {
    func configure(keyboard config: KeyboardConfig) {
        if let autocapitalizationType = config.autocapitalizationType {
            self.autocapitalizationType = autocapitalizationType
        }
        if let isSecureTextEntry = config.isSecureTextEntry {
            self.isSecureTextEntry = isSecureTextEntry
        }
        if let keyboardType = config.keyboardType {
            self.keyboardType = keyboardType
        }
        if let spellCheckingType = config.spellCheckingType {
            self.spellCheckingType = spellCheckingType
        }
        if let autocorrectionType = config.autocorrectionType {
            self.autocorrectionType = autocorrectionType
        }
    }
}
