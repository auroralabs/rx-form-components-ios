import UIKit

public struct TextFieldConfig {
    public let label: String?
    public let placeholder: String?
    public let keyboardConfig: KeyboardConfig
    public let returnKey: UIReturnKeyType

    public init(
        label: String? = nil,
        placeholder: String? = nil,
        keyboardConfig: KeyboardConfig,
        returnKey: UIReturnKeyType
    ) {
        self.label = label
        self.placeholder = placeholder
        self.keyboardConfig = keyboardConfig
        self.returnKey = returnKey
    }
}
