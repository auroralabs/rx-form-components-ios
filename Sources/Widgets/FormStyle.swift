import UIKit

public enum DefaultStyle {
    public static let labelMargin: CGFloat = 5.0
    public static let inputHeight: CGFloat = 40.0
    public static let textViewMinHeight: CGFloat = 195.0
    public static let errorMargin: CGFloat = 5.0
    public static let topLabelColor = #colorLiteral(red: 0.462745098, green: 0.462745098, blue: 0.462745098, alpha: 1)
    public static let disabledLabelColor = #colorLiteral(red: 0.462745098, green: 0.462745098, blue: 0.462745098, alpha: 1)
    public static let errorColor = #colorLiteral(red: 0.9098039216, green: 0.2431372549, blue: 0.2588235294, alpha: 1)
    public static let borderColor = #colorLiteral(red: 0.8745098039, green: 0.8745098039, blue: 0.8745098039, alpha: 1)
    public static let cornerRadius: CGFloat = 6.0
    public static let topLabelFont: UIFont = UIFont.systemFont(ofSize: 14.0)
    public static let errorLabelFont = UIFont.systemFont(ofSize: 14.0)
    public static let inputFont = UIFont.systemFont(ofSize: 16.0)
    public static let inputColor = UIColor.black
    public static let placeholderColor = #colorLiteral(red: 0.462745098, green: 0.462745098, blue: 0.462745098, alpha: 1)
    public static let inputInsets = UIEdgeInsets(top: 9, left: 7, bottom: 9, right: 7)
    public static let toolbarHeight: CGFloat = 44.0
}

public class FormStyle: NSObject, NSCopying {


    
    var labelMargin: CGFloat
    var inputHeight: CGFloat
    var textViewMinHeight: CGFloat
    var errorMargin: CGFloat
    var topLabelColor: UIColor
    var disabledLabelColor: UIColor
    var errorColor: UIColor
    var borderColor: UIColor
    var cornerRadius: CGFloat
    var topLabelFont: UIFont
    var errorLabelFont: UIFont
    var inputFont: UIFont
    var inputColor: UIColor
    var placeholderColor: UIColor
    var inputInsets: UIEdgeInsets
    
    public init(
        labelMargin: CGFloat = DefaultStyle.labelMargin,
        inputHeight: CGFloat = DefaultStyle.inputHeight,
        textViewMinHeight: CGFloat = DefaultStyle.textViewMinHeight,
        errorMargin: CGFloat = DefaultStyle.errorMargin,
        topLabelColor: UIColor = DefaultStyle.topLabelColor,
        disabledLabelColor: UIColor = DefaultStyle.disabledLabelColor,
        errorColor: UIColor = DefaultStyle.errorColor,
        borderColor: UIColor = DefaultStyle.borderColor,
        cornerRadius: CGFloat = DefaultStyle.cornerRadius,
        topLabelFont: UIFont = DefaultStyle.topLabelFont,
        errorLabelFont: UIFont = DefaultStyle.errorLabelFont,
        inputFont: UIFont = DefaultStyle.inputFont,
        inputColor: UIColor = DefaultStyle.inputColor,
        placeholderColor: UIColor = DefaultStyle.placeholderColor,
        inputInsets: UIEdgeInsets = DefaultStyle.inputInsets
    ) {
        self.labelMargin = labelMargin
        self.inputHeight = inputHeight
        self.textViewMinHeight = textViewMinHeight
        self.errorMargin = errorMargin
        self.topLabelColor = topLabelColor
        self.disabledLabelColor = disabledLabelColor
        self.errorColor = errorColor
        self.borderColor = borderColor
        self.cornerRadius = cornerRadius
        self.topLabelFont = topLabelFont
        self.errorLabelFont = errorLabelFont
        self.inputFont = inputFont
        self.inputColor = inputColor
        self.placeholderColor = placeholderColor
        self.inputInsets = inputInsets
    }
    
    public func copy(with zone: NSZone? = nil) -> Any {
        return FormStyle(
            labelMargin: self.labelMargin,
            inputHeight: self.inputHeight,
            textViewMinHeight: self.textViewMinHeight,
            errorMargin: self.errorMargin,
            topLabelColor: self.topLabelColor,
            disabledLabelColor: self.disabledLabelColor,
            errorColor: self.errorColor,
            borderColor: self.borderColor,
            cornerRadius: self.cornerRadius,
            topLabelFont: self.topLabelFont,
            errorLabelFont: self.errorLabelFont,
            inputFont: self.inputFont,
            inputColor: self.inputColor,
            placeholderColor: self.placeholderColor,
            inputInsets: self.inputInsets
        )
    }

    @available(*, unavailable, message: "Use RxFormComponents.appearance().style = FormStyle(topLabelColor: UIColor.red) instead")
    public func applyStyle() {}
}

extension UILabel {
    static func top(style: FormStyle?) -> UILabel {
        let label = UILabel()
        label.textColor = style?.topLabelColor ?? DefaultStyle.topLabelColor
        label.numberOfLines = 0
        label.font = style?.topLabelFont ?? DefaultStyle.topLabelFont
        return label
    }

    static func error(style: FormStyle?) -> UILabel {
        let label = UILabel()
        label.textColor = style?.errorColor ?? DefaultStyle.errorColor
        label.numberOfLines = 0
        label.font = style?.errorLabelFont ?? DefaultStyle.errorLabelFont
        return label
    }

    static func placeholder(style: FormStyle?) -> UILabel {
        let label = UILabel()
        label.textColor = style?.placeholderColor ?? DefaultStyle.placeholderColor
        label.numberOfLines = 0
        label.font = style?.inputFont ?? DefaultStyle.inputFont
        return label
    }
}
