import UIKit
import RxCocoa
import RxSwift

@IBDesignable open class ValidatableTextField: BaseTextFieldContainer {

}

public extension ValidatableTextField {
    func configure(
        label: String? = nil,
        placeholder: String? = nil,
        keyboardConfig: KeyboardConfig,
        returnKey: UIReturnKeyType = .default,
        next: ValidatableTextField? = nil,
        delegate: UITextFieldDelegate? = nil
    ) {
        textField.configure(keyboard: keyboardConfig)
        textField.returnKeyType = returnKey
        textField.nextTextField = next?.textField
        textField.delegate = delegate
        self.placeholder = placeholder
        self.label = label
    }

    func configure(_ config: TextFieldConfig) {
        textField.configure(keyboard: config.keyboardConfig)
        self.label = config.label
        self.placeholder = config.placeholder
        textField.returnKeyType = config.returnKey
    }

    func set(next: ValidatableTextField?, delegate: UITextFieldDelegate?) {
        textField.delegate = delegate
        textField.nextTextField = next?.textField
    }
}


extension FormInput where T == String {
    public func drive(_ textField: ValidatableTextField) -> Disposable {
        CompositeDisposable(
            error.drive(textField.rx.error),
            value.drive(textField.textField.rx.text),
            textField.textField.rx.text.bind(onNext: { [weak self] value in self?.didChange(value: value) }),
            textField.textField.rx.controlEvent(.editingDidEnd).withLatestFrom(textField.textField.rx.text)
                .bind(onNext: { [weak self] value in self?.didEndEditing(value: value) })
        )
    }
}

extension UITextField {
    func configure(keyboard config: KeyboardConfig) {
        if let autocapitalizationType = config.autocapitalizationType {
            self.autocapitalizationType = autocapitalizationType
        }
        if let isSecureTextEntry = config.isSecureTextEntry {
            self.isSecureTextEntry = isSecureTextEntry
        }
        if let keyboardType = config.keyboardType {
            self.keyboardType = keyboardType
        }
        if let spellCheckingType = config.spellCheckingType {
            self.spellCheckingType = spellCheckingType
        }
        if let autocorrectionType = config.autocorrectionType {
            self.autocorrectionType = autocorrectionType
        }
        if let contentType = config.contentType {
            self.textContentType = contentType
        }
    }
}
