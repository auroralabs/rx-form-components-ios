import UIKit
import RxSwift
import RxCocoa

extension FormInput where T == Bool {
    public func drive(_ input: UISwitch) -> Disposable {
        CompositeDisposable(
            value.map { $0 ?? false }.drive(input.rx.isOn),
            input.rx.isOn.bind(onNext: { [weak self] value in self?.didChange(value: value) }),
            input.rx.controlEvent(.editingDidEnd).withLatestFrom(input.rx.isOn)
                .bind(onNext: { [weak self] value in self?.didEndEditing(value: value) })
        )
    }
}
