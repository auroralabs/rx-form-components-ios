import UIKit
import RxSwift
import RxCocoa

@IBDesignable
open class ValidatablePickerView: BaseTextFieldContainer {

    private let picker = UIPickerView()
    private var doneButton: UIBarButtonItem?
    private var toolbar: UIToolbar?
    private let disposeBag = DisposeBag()
    fileprivate let outputSelectedRowRelay: BehaviorRelay<Int?> = .init(value: nil)
    fileprivate let inputSelectedRowRelay: BehaviorRelay<Int?> = .init(value: nil)

    public var items: [String] = [] { didSet { picker.reloadAllComponents() } }
    public var selectedItem: String? { didSet { textField.text = selectedItem } }
    public var isEnabled: Bool {
        set { textField.isEnabled = newValue }
        get { textField.isEnabled }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }

    private func setUp() {
        textField.inputView = picker
        textField.clearButtonMode = .always
        picker.dataSource = self
        picker.delegate = self
        addRightButton(type: .dropDown, target: self, action: #selector(rightButtonTapped))

        inputSelectedRowRelay.distinctUntilChanged().bind { [weak self] row in
            guard let self = self else { return }
            if let row = row, let item = self.items[safeIndex: row] {
                self.selectedItem = item
                self.picker.selectRow(row, inComponent: 0, animated: false)
            } else {
                self.selectedItem = nil
            }
        }.disposed(by: disposeBag)

        textField.rx.controlEvent(.editingDidBegin).bind { [weak self] _ in
            guard self?.selectedItem == nil else { return }
            self?.selectedItem = self?.items[safeIndex: 0]
            self?.outputSelectedRowRelay.accept(0)
        }.disposed(by: disposeBag)
    }
    
    @objc private func rightButtonTapped() {
        textField.becomeFirstResponder()
    }

    public func configure(label: String, placeholder: String, showToolbar: Bool = false) {
        self.label = label
        self.placeholder = placeholder
        
        if showToolbar {
            self.doneButton = .init(barButtonSystemItem: .done, target: self, action: nil)
            self.toolbar = UIToolbar.init(frame: CGRect.init(x: 0, y: 0, width: picker.frame.width, height: DefaultStyle.toolbarHeight))
            if let doneButton = self.doneButton, let toolbar = self.toolbar {
                textField.inputAccessoryView = toolbar
                toolbar.setItems([doneButton], animated: false)
                doneButton.rx.tap.bind { [weak self] in
                    self?.textField.resignFirstResponder()
                }.disposed(by: disposeBag)
            }
        }
    }
}

extension ValidatablePickerView: UIPickerViewDataSource {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int { 1 }

    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int { items.count }
}

extension ValidatablePickerView: UIPickerViewDelegate {
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        items[safeIndex: row]
    }

    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedItem = items[safeIndex: row]
        outputSelectedRowRelay.accept(row)
    }
}

extension SelectorFormInput where T: InputValueRepresentable {

    public func drive(_ picker: ValidatablePickerView) -> Disposable {
        CompositeDisposable(
            error.drive(picker.rx.error),
            items.drive(onNext: { [weak picker] items in picker?.items = items.map { $0.valueRepresentation } }),
            index.drive(picker.inputSelectedRowRelay),
            picker.outputSelectedRowRelay.skip(1).bind(onNext: { [weak self] index in self?.didSelectItem(at: index) })
        )
    }
}
