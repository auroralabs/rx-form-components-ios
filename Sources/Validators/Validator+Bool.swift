import Foundation

public extension Validator where T == Bool {

    static func empty(message: String?) -> Validator<T> {
        .closure(error: message, validation: { $0 == nil })
    }

    static func mandatory(message: String?) -> Validator<T> {
        .closure(error: message, validation: { $0 != nil })
    }

    static func `true`(message: String?) -> Validator<T> {
        .closure(error: message, validation: { $0 == true })
    }

    static func `false`(message: String?) -> Validator<T> {
        .closure(error: message, validation: { $0 == false })
    }
}
