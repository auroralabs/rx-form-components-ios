import Foundation

open class Validator<T: Any> {
    public func validate(_ value: T?) -> Result { return .success }
}

extension Validator {
    public enum Result {
        case success
        case error(String?)

        public var isValid: Bool {
            switch self {
            case .success: return true
            case .error: return false
            }
        }

        public var error: String? {
            switch self {
            case .success: return nil
            case .error(let error): return error
            }
        }
    }
}

public extension Validator {
    static func closure(error: String?, validation: @escaping (T?) -> Bool) -> Validator<T> {
        ClosureValidator(validation: validation, errorMessage: error)
    }

    static func and(_ validations: Validator<T>...) -> Validator<T> {
        ANDValidator(validations: validations)
    }

    static func or(_ validations: Validator<T>...) -> Validator<T> {
        ORValidator(validations: validations)
    }

    static func not(validation: Validator<T>, error: String?) -> Validator<T> {
        NOTValidator(validation: validation, errorMessage: error)
    }
}

class ClosureValidator<T: Any>: Validator<T> {
    let validation: (T?) -> Bool
    let errorMessage: String?

    public init(validation: @escaping (T?) -> Bool, errorMessage: String?) {
        self.validation = validation
        self.errorMessage = errorMessage
        super.init()
    }

    public override func validate(_ value: T?) -> Result { validation(value) ? .success : .error(errorMessage) }
}

class ANDValidator<T: Any>: Validator<T> {
    let validations: [Validator<T>]

    public init(validations: [Validator<T>]) {
        self.validations = validations
        super.init()
    }

    public override func validate(_ value: T?) -> Result {
        for validation in validations {
            let result = validation.validate(value)
            guard result.isValid else { return result }
        }
        return .success
    }
}

class ORValidator<T: Any>: Validator<T> {
    let validations: [Validator<T>]

    public init(validations: [Validator<T>]) {
        self.validations = validations
        super.init()
    }

    public override func validate(_ value: T?) -> Result {
        var lastResult: Result?
        for validation in validations {
            let result = validation.validate(value)
            guard !result.isValid else { return .success }
            lastResult = result
        }
        return lastResult ?? .success
    }
}

class NOTValidator<T: Any>: Validator<T> {
    let validation: Validator<T>
    let errorMessage: String?

    public init(validation: Validator<T>, errorMessage: String?) {
        self.errorMessage = errorMessage
        self.validation = validation
        super.init()
    }

    public override func validate(_ value: T?) -> Result {
        validation.validate(value).isValid ? .error(errorMessage) : .success
    }
}
