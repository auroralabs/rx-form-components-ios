import Foundation

public extension Validator where T == String {

    static func empty(message: String?) -> Validator<T> {
        .closure(error: message, validation: { $0?.isEmpty ?? true })
    }

    static func mandatory(message: String?) -> Validator<T> {
        .closure(error: message, validation: { !($0?.isEmpty ?? true) })
    }

    static func regex(_ regex: String, message: String?) -> Validator<T> {
        .closure(error: message, validation: { ($0 ?? "")
            .range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil })
    }

    static func regex(_ regex: RegEx, message: String?) -> Validator<T> {
        .closure(error: message, validation: { ($0 ?? "")
            .range(of: regex.rawValue, options: .regularExpression, range: nil, locale: nil) != nil })
    }

    static func email(message: String?) -> Validator<T> {
        .closure(error: message, validation: {
            ($0?.lowercased() ?? "")
                .range(of: RegEx.email.rawValue, options: .regularExpression, range: nil, locale: nil) != nil
        })
    }

    static func length(range: CountableClosedRange<Int>, message: String?) -> Validator<T> {
        .closure(error: message) { range.contains($0?.count ?? 0) }
    }

    static func length(_ length: Int, message: String?) -> Validator<T> {
        .closure(error: message) { ($0?.count ?? 0) == length }
    }

    static func minLength(length: Int, message: String?) -> Validator<T> {
        .closure(error: message) { ($0?.count ?? 0) >= length }
    }

    static func url(message: String?) -> Validator<T> { .regex(.url, message: message) }
}

public extension Validator {
    enum RegEx: String {
        case email = "^(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])$"
        case positiveInteger = "^\\d+$"
        case url = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)"
        case location = "^\\(?[-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?),\\s?[-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?\\)?)$"
    }
}
