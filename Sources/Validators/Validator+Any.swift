import Foundation

public extension Validator where T: Any {

    static func mandatory(message: String?) -> Validator<T> {
        .closure(error: message, validation: { $0 != nil })
    }

    static func `nil`(message: String?) -> Validator<T> {
        .closure(error: message, validation: { $0 == nil })
    }
}

public extension Validator where T: Equatable {

    static func equal(_ other: T, message: String?) -> Validator<T> {
        .closure(error: message) { other == $0 }
    }

    static func equal(closure: @escaping () -> T, message: String?) -> Validator<T> {
        .closure(error: message) { closure() == $0 }
    }
}

public extension Validator where T: Comparable {

    static func greaterOrEqual(_ other: T, message: String?) -> Validator<T> {
        .closure(error: message) {
            guard let rhs = $0 else { return false }
            return rhs >= other
        }
    }

    static func greater(_ other: T, message: String?) -> Validator<T> {
        .closure(error: message) {
            guard let rhs = $0 else { return false }
            return rhs > other
        }
    }

    static func greaterOrEqual(closure: @escaping () -> T?, message: String?) -> Validator<T> {
        .closure(error: message) {
            guard let rhs = $0, let lhs = closure() else { return false }
            return rhs >= lhs
        }
    }

    static func greater(closure: @escaping () -> T?, message: String?) -> Validator<T> {
        .closure(error: message) {
            guard let rhs = $0, let lhs = closure() else { return false }
            return rhs > lhs
        }
    }

    static func lessOrEqual(_ other: T, message: String?) -> Validator<T> {
        .closure(error: message) {
            guard let rhs = $0 else { return false }
            return rhs <= other
        }
    }

    static func less(_ other: T, message: String?) -> Validator<T> {
        .closure(error: message) {
            guard let rhs = $0 else { return false }
            return rhs < other
        }
    }

    static func lessOrEqual(than other: @escaping () -> T?, message: String?) -> Validator<T> {
        .closure(error: message) {
            guard let rhs = $0, let lhs = other() else { return false }
            return rhs <= lhs
        }
    }

    static func less(than other: @escaping () -> T?, message: String?) -> Validator<T> {
        .closure(error: message) {
            guard let rhs = $0, let lhs = other() else { return false }
            return rhs < lhs
        }
    }
}
