import Foundation

public extension Validator where T == Date {

    static func empty(message: String) -> Validator<T> {
        .closure(error: message, validation: { $0 == nil })
    }

    static func mandatory(message: String) -> Validator<T> {
        .closure(error: message, validation: { $0 != nil })
    }

    static func included(range: Range<T>, message: String) -> Validator<T> {
        .closure(error: message) {
            guard let date = $0 else { return false }
            return range.contains(date)
        }
    }
}
