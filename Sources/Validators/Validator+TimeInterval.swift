import Foundation

public extension Validator where T == TimeInterval {

    static func empty(message: String?) -> Validator<T> {
        .closure(error: message, validation: { $0 == nil })
    }

    static func mandatory(message: String?) -> Validator<T> {
        .closure(error: message, validation: { $0 != nil })
    }

    static func included(range: Range<TimeInterval>, message: String?) -> Validator<T> {
        .closure(error: message) {
            guard let time = $0 else { return false }
            return range.contains(time)
        }
    }
}
