import UIKit

public extension UIButton {

    var title: String? {
        set { setTitle(newValue, for: .normal) }
        get { title(for: .normal) }
    }

    func setInsets(imageTitlePadding: CGFloat, contentPadding: UIEdgeInsets = .zero) {
        self.contentEdgeInsets = UIEdgeInsets(
            top: contentPadding.top,
            left: contentPadding.left,
            bottom: contentPadding.bottom,
            right: contentPadding.right + imageTitlePadding
        )
        self.titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: imageTitlePadding,
            bottom: 0,
            right: -imageTitlePadding
        )
    }
}
