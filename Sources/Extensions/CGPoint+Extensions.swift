import UIKit

public extension CGPoint {
    init(x: CGFloat) {
        self.init(x: x, y: 0)
    }

    init(y: CGFloat) {
        self.init(x: 0, y: y)
    }
}

public func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

public func - (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}
