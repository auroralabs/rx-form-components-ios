import UIKit

public extension CGRect {
    init(size: CGSize) {
        self.init(origin: .zero, size: size)
    }

    var topLeft: CGPoint {
        get { origin }
        set { origin = newValue }
    }

    var topRight: CGPoint {
        get { origin + .init(x: width, y: 0) }
        set { origin = newValue - .init(x: width, y: 0) }
    }

    var topCenter: CGPoint {
        get { origin + .init(x: width / 2.0, y: 0) }
        set { origin = newValue - .init(x: width / 2.0, y: 0) }
    }

    var midLeft: CGPoint {
        get { origin + .init(x: 0, y: height / 2.0) }
        set { origin = newValue - .init(x: 0, y: height / 2.0) }
    }

    var midRight: CGPoint {
        get { origin + .init(x: width, y: height / 2.0) }
        set { origin = newValue - .init(x: width, y: height / 2.0) }
    }

    var bottomLeft: CGPoint {
        get { origin + .init(x: 0, y: height) }
        set { origin = newValue - .init(x: 0, y: height) }
    }

    var bottomRight: CGPoint {
        get { origin + .init(x: width, y: height) }
        set { origin = newValue - .init(x: width, y: height) }
    }

    var bottomCenter: CGPoint {
        get { origin + .init(x: width / 2.0, y: height) }
        set { origin = newValue - .init(x: width / 2.0, y: height) }
    }

    var center: CGPoint {
        get { origin + .init(x: width / 2.0, y: height / 2.0) }
        set { origin = newValue - .init(x: width / 2.0, y: height / 2.0) }
    }
}

