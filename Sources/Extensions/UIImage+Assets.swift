import UIKit

private class AssetsBundle {
    static var bundle: Bundle? {
        guard let url = Bundle(for: self).url(forResource: "RxFormComponents", withExtension: "bundle") else { return nil }
        return Bundle(url: url)
    }
}


extension UIImage {

    static func named(_ name: String) -> UIImage? {
        UIImage(named: name, in: AssetsBundle.bundle, compatibleWith: nil)
    }
}

extension UIImage {
    static var disclosureIcn: UIImage? { named("disclosureIcn") }
    static var dropdownIcn: UIImage? { named("dropdownIcn") }
    static var infoIcn: UIImage? { named("infoIcn") }
}
