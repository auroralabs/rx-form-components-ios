import Foundation

public class RxFormComponents {

    public class Appearance {
        public var style: FormStyle = .init() {
            didSet {
                ValidatableTextField.appearance().style = style
                ValidatableSelectorButton.appearance().style = style
                ValidatableTextView.appearance().style = style
                ValidatablePickerView.appearance().style = style
                ValidatableDatePicker.appearance().style = style
            }
        }
    }

    private static var _appearance: Appearance = .init()

    public static func appearance() -> Appearance { _appearance }
}
