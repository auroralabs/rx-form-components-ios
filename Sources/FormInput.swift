import Foundation
import RxSwift
import RxCocoa

open class FormInput<T> {
    public private(set) var value: Driver<T?>
    public private(set) var error: Driver<String?>

    init(value: Driver<T?>, error: Driver<String?>) {
        self.value = value
        self.error = error
    }

    public func didEndEditing(value: T?) {}
    public func didChange(value: T?) {}
}

public protocol ValidatableInput {
    func validate() -> Bool
}

open class FormInputModel<T>: FormInput<T>, ValidatableInput {

    public var valueRelay: BehaviorRelay<T?>
    public var errorRelay: BehaviorRelay<String?>
    public let validation: Validator<T>?

    public init(value: T? = nil, error: String? = nil, validation: Validator<T>? = nil) {
        self.valueRelay = BehaviorRelay(value: value)
        self.errorRelay = BehaviorRelay(value: error)
        self.validation = validation
        super.init(value: valueRelay.asDriver(), error: errorRelay.asDriver())
    }

    @discardableResult
    public func validate() -> Bool {
        guard let validation = validation else { return true }
        let result = validation.validate(valueRelay.value)
        errorRelay.accept(result.error)
        return result.isValid
    }

    public override func didEndEditing(value: T?) {
        valueRelay.accept(value)
        validate()
    }

    public override func didChange(value: T?) {
        valueRelay.accept(value)
        if errorRelay.value != nil { validate() }
    }
}

open class SelectorFormInput<T: Equatable>: FormInput<T> {

    public private(set) var items: Driver<[T]>
    public var index: Driver<Int?> {
        Driver.combineLatest(items, value).map { items, value in
            guard let value = value else { return nil }
            return items.firstIndex(of: value)
        }.distinctUntilChanged()
    }

    init(items: Driver<[T]>, value: Driver<T?>, error: Driver<String?>) {
        self.items = items
        super.init(value: value, error: error)
    }

    public override func didEndEditing(value: T?) {}
    public override func didChange(value: T?) {}
    public func didSelectItem(at index: Int?) {}
}

open class SelectorFormInputModel<T: Equatable>: SelectorFormInput<T>, ValidatableInput {

    public let itemsRelay: BehaviorRelay<[T]>
    public let valueRelay: BehaviorRelay<T?>
    public let errorRelay: BehaviorRelay<String?>
    public let validation: Validator<T>?

    public init(items: [T], value: T? = nil, error: String? = nil, validation: Validator<T>? = nil) {
        self.itemsRelay = BehaviorRelay(value: items)
        self.valueRelay = BehaviorRelay(value: value)
        self.errorRelay = BehaviorRelay(value: error)
        self.validation = validation
        super.init(items: itemsRelay.asDriver(), value: valueRelay.asDriver(), error: errorRelay.asDriver())
    }

    @discardableResult
    public func validate() -> Bool {
        guard let validation = validation else { return true }
        let result = validation.validate(valueRelay.value)
        errorRelay.accept(result.error)
        return result.isValid
    }

    public override func didEndEditing(value: T?) {
        valueRelay.accept(value)
        validate()
    }

    public override func didChange(value: T?) {
        valueRelay.accept(value)
        if errorRelay.value != nil { validate() }
    }

    public override func didSelectItem(at index: Int?) {
        guard let index = index else { didChange(value: nil); return }
        didChange(value: itemsRelay.value[safeIndex: index])
    }

    public func updateItems(items: [T]) {
        itemsRelay.accept(items)
        if let currentValue = valueRelay.value, !items.contains(currentValue) {
            didChange(value: nil)
        }
    }
}

public protocol InputValueRepresentable {
    var valueRepresentation: String { get }
}

public extension Array where Element == ValidatableInput {
    func validate() -> Bool {
        let validations = map { $0.validate() } // We need to validate all of them, using reduce stops in the first error
        return validations.reduce(true) { $0 && $1 }
    }
}
