import Foundation
import RxCocoa
import RxSwift
import RxFormComponents

protocol BasicFormViewModelInput { }

protocol BasicFormViewModelOutput {
    var name: FormInput<String> { get }
    var surname: FormInput<String> { get }
    var birthdate: FormInput<Date> { get }
    var gender: SelectorFormInput<String> { get }
    var biography: FormInput<String> { get }
    var maximunDate: Driver<Date> { get }
}

protocol BasicFormViewModelType {
    var input: BasicFormViewModelInput { get }
    var output: BasicFormViewModelOutput { get }
    var navigator: BasicFormNavigator? { get set }
}

final class BasicFormViewModel: BasicFormViewModelType {
    var input: BasicFormViewModelInput { return self }
    var output: BasicFormViewModelOutput { return self }    
    weak var navigator: BasicFormNavigator?

    private let dependencies: Dependencies
    private let form: BasicForm

    init(dependencies: Dependencies = Dependencies()) {
        self.dependencies = dependencies
        self.form = .init(
            name: .init(
                validation: .and(
                    .mandatory(message: "Campo obligatorio"),
                    .length(range: 3...20, message: "Debe contener entre 3 y 20 caracteres")
                )
            ),
            surname: .init(value: "Murillo", validation: .mandatory(message: "Campo obligatorio")),
            birthdate: .init(
                validation: .and(
                    .mandatory(message: "Campo obligatorio"),
                    .lessOrEqual(Date(), message: "La fecha debe ser anterior a la de hoy")
                )
            ),
            gender: .init(items: ["Hombre", "Mujer"], validation: .mandatory(message: "Campo obligatorio")),
            biography: .init(validation: .mandatory(message: "Campo obligatorio"))
        )
    }
}

extension BasicFormViewModel: BasicFormViewModelInput { }

extension BasicFormViewModel: BasicFormViewModelOutput {
    var name: FormInput<String> { form.name }
    var surname: FormInput<String> { form.surname }
    var birthdate: FormInput<Date> { form.birthdate }
    var gender: SelectorFormInput<String> { form.gender }
    var biography: FormInput<String> { form.biography }
    var maximunDate: Driver<Date> { form.birthdate.value.map { $0?.byAdding(days: 3) ?? Date() } }
}

extension BasicFormViewModel {
    struct Dependencies {}
}


struct BasicForm {
    let name: FormInputModel<String>
    let surname: FormInputModel<String>
    let birthdate: FormInputModel<Date>
    let gender: SelectorFormInputModel<String>
    let biography: FormInputModel<String>
}

extension String: InputValueRepresentable {
    public var valueRepresentation: String { self }
}

extension Date {
    func byAdding(days: Int) -> Date {
        var components = DateComponents()
        components.day = days
        return Calendar.current.date(byAdding: components, to: self) ?? self
    }
}
