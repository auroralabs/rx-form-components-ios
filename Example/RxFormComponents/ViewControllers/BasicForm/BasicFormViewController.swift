import UIKit
import RxCocoa
import RxSwift
import RxFormComponents

final class BasicFormViewController: UIViewController {

    @IBOutlet var nameTextField: ValidatableTextField!
    @IBOutlet var surnameTextField: ValidatableTextField!
    @IBOutlet var birthdateDatePicker: ValidatableDatePicker!
    @IBOutlet var genderPicker: ValidatablePickerView!
    @IBOutlet var biographyTextView: ValidatableTextView!

    var viewModel: BasicFormViewModelType? { didSet { viewModel?.navigator = self } }
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = BasicFormViewModel()
        setUpView()
        localize()
        bind()        
    }

    private func setUpView() {
        nameTextField.configure(
            .init(
                label: "Nombre",
                placeholder: "Escriba su nombre",
                keyboardConfig: .name,
                returnKey: .next
            )
        )
        surnameTextField.configure(
            .init(
                label: "Apellidos",
                placeholder: "Escriba sus apellidos",
                keyboardConfig: .name,
                returnKey: .next
            )
        )
        birthdateDatePicker.configure(
            config: .init(
                label: "Fecha de nacimiento",
                placeholder: "Seleccione su fecha de nacimiento",
                mode: .date,
                maximumDate: Date()
            )
        )
        genderPicker.configure(label: "Género", placeholder: "Seleccione su género", showToolbar: true)
        biographyTextView.configure(
            label: "Biografía",
            placeholder: "Escriba su biografía",
            keyboardConfig: .sentence
        )
    }

    private func localize() { }

    private func bind() {
    	// Output
        viewModel?.output.name.drive(nameTextField).disposed(by: disposeBag)
        viewModel?.output.surname.drive(surnameTextField).disposed(by: disposeBag)
        viewModel?.output.birthdate.drive(birthdateDatePicker).disposed(by: disposeBag)
        viewModel?.output.gender.drive(genderPicker).disposed(by: disposeBag)
        viewModel?.output.biography.drive(biographyTextView).disposed(by: disposeBag)
        viewModel?.output.maximunDate.drive(onNext: { [weak self] date in
            self?.birthdateDatePicker.datePicker.maximumDate = date
        }).disposed(by: disposeBag)

    	// Input
    }
}
