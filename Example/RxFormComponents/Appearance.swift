//
//  Appearance.swift
//  RxFormComponents_Example
//
//  Created by Luis Manuel Cotrino Benavides on 13/4/21.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import UIKit
import RxFormComponents

final class Appearance {
    
    static func applyDefaultAppearance() {
        // Change one component style at a time:
//        let textFieldCustomAppearance = ValidatableTextField.appearance()
//        textFieldCustomAppearance.style = FormStyle(
//            topLabelColor: UIColor.red,
//            topLabelFont: UIFont.systemFont(ofSize: 20, weight: .bold),
//            inputFont: UIFont.systemFont(ofSize: 20, weight: .regular)
//        )
//
//        let selectorButtonAppearance = ValidatableSelectorButton.appearance()
//        selectorButtonAppearance.style = FormStyle(
//            topLabelColor: UIColor.red,
//            borderColor: UIColor.green
//        )
//
//        let textViewAppearance = ValidatableTextView.appearance()
//        textViewAppearance.style = FormStyle(
//            topLabelColor: UIColor.red,
//            borderColor: UIColor.green
//        )

        // Or all at once:
        RxFormComponents.appearance().style = .init(
            labelMargin: 8,
            topLabelColor: .black,
            cornerRadius: 12,
            topLabelFont: UIFont.systemFont(ofSize: 13, weight: .semibold),
            inputFont: UIFont.systemFont(ofSize: 16)
        )
    }

}
