// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "RxFormComponents",
    platforms: [
        .iOS(.v11),
    ],
    products: [
        .library(
            name: "RxFormComponents",
            targets: ["RxFormComponents"]),
    ],
    dependencies: [
        .package(url: "https://github.com/ReactiveX/RxSwift", .upToNextMajor(from: "6.0.0")),
    ],
    targets: [
        .target(
            name: "RxFormComponents",
            dependencies: [
                .product(name: "RxSwift", package: "RxSwift"),
                .product(name: "RxCocoa", package: "RxSwift"),
            ],
            path: "Sources",
            resources: [
                .copy("../Resources/Assets.xcassets")
            ]
        ),
    ],
    swiftLanguageVersions: [.v5]
)
